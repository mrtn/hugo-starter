---
title: Privacy
header: Privacy
draft: false
---

By browsing this site you agree that privacy is a good thing. We do not spy on you. We may use cookies. This is the internet. It is OK to use cookies.
