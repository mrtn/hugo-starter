# My site

### Run a development environment

```bash
$ npm start
```

### Build a production copy

```bash
$ npm run build
```

Stuff